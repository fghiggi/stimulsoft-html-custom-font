﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using Stimulsoft.Report;
using Stimulsoft.Report.Mvc;

using stimulsoft_html_custom_font.Models;

using System.Diagnostics;

namespace stimulsoft_html_custom_font.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        private StiReport GetDashboard()
        {
            var reportPath = StiNetCoreHelper.MapPath(this, "Reports/Report.mrt");
            var report = new StiReport();

            report.Load(reportPath);

            return report;
        }

        public IActionResult ExportHtml()
        {
            var report = GetDashboard();

            return StiNetCoreReportResponse.ResponseAsHtml(report, saveFileDialog: false);
        }

        public IActionResult ExportPdf()
        {
            var report = GetDashboard();

            return StiNetCoreReportResponse.ResponseAsPdf(report, saveFileDialog: false);
        }
    }
}
